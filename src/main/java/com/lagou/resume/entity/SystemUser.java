package com.lagou.resume.entity;

import java.io.Serializable;

public class SystemUser implements Serializable {
    private String username;
    private String password;
    private String token;
    private String salt;
    public static final String defaultToken = "lagou$2019";

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }
}
