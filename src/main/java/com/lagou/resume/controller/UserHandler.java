package com.lagou.resume.controller;


import com.lagou.resume.entity.SystemUser;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * 拦截器
 */
public class UserHandler extends HandlerInterceptorAdapter {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HttpSession session = request.getSession();
        String token = (String) session.getAttribute("auth_token");
        if (StringUtils.isBlank(token)) {
            response.sendRedirect("/login");
            return false;
        }
        if (!SystemUser.defaultToken.equalsIgnoreCase(token)) {
            response.sendRedirect("/login");
            return false;
        }
        return true;
    }
}
