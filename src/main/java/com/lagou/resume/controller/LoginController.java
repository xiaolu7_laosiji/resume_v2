package com.lagou.resume.controller;


import com.lagou.resume.entity.SystemUser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

/**
 * 登录controller
 */
@Controller
@RequestMapping
public class LoginController {

    @RequestMapping("/sessionId")
    @ResponseBody
    public Object sessionId(HttpSession session) {
        Map map = new HashMap<>();
        map.put("threadId", Thread.currentThread().getId());
        map.put("sessionId", session.getId());
        return map;
    }

    @RequestMapping("/login")
    public String login(HttpSession session) {
        String token = (String) session.getAttribute("auth_token");
        if ("lagou$2019".equalsIgnoreCase(token)) {
            return "redirect:/resume/index";
        }
        return "/login";
    }

    @RequestMapping("/doLogin")
    @ResponseBody
    public boolean doLogin(SystemUser user, RedirectAttributes r, HttpSession session) {
        if ("admin".equals(user.getUsername()) && "admin".equals(user.getPassword())) {
            session.setAttribute("auth_token", "lagou$2019");
            return true;
        }
        return false;
    }
}
